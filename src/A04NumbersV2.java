import java.util.Scanner;

/**
 * Author: derijkej
 */
public class A04NumbersV2 {
	public static void main(String[] args) {
		final int MINIMUM = 100_000;
		final int MAXIMUM = 999_999;

		Scanner keyboard = new Scanner(System.in);
		System.out.print("Please enter a 6 digit number: ");
		long number1 = keyboard.nextInt();
		System.out.print("Please enter another 6 digit number: ");
		long number2 = keyboard.nextInt();
		if ((number1 >= MINIMUM) && (number1 <= MAXIMUM) && (number2 >= MINIMUM) && (number2 <= MAXIMUM)) {
			System.out.println("intermediate: "+ (number1 * number2));
			System.out.println((number1 * number2)%100_000);
		}else{
			System.err.println("Please enter 6 digit numbers");
		}
	}
}
